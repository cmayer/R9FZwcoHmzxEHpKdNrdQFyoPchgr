### Data and code associated with the study

**Lineage congruence, divergence, and convergence in the mouse telencephalon**  
Rachel C. Bandler<sup>1,2,3,#</sup>, Ryan N. Delgado<sup>4,5,6,#</sup>, Josue S. Ibarra<sup>1</sup>, Ilaria Vitali<sup>1</sup>, May C. Ho<sup>1</sup>, Paul Frazel<sup>2</sup>, Maesoumeh Mohammadkhani<sup>2</sup>, Robert Machold<sup>2</sup>, Shane Liddelow<sup>2,7,8</sup>, Tomasz J. Nowakowski<sup>5,6</sup>, Gord Fishell<sup>3,9</sup>, Christian Mayer<sup>1*</sup>

<sup>1</sup>Max Planck Institute of Neurobiology, Martinsried, Germany  
<sup>2</sup>NYU Neuroscience Institute, Langone Medical Center, New York, NY, USA  
<sup>3</sup>Broad Institute, Stanley Center for Psychiatric Research, Cambridge MA, USA  
<sup>4</sup>Department of Anatomy, University of California, San Francisco, CA, USA  
<sup>5</sup>Department of Psychiatry, University of California, San Francisco, CA, USA  
<sup>6</sup>Eli and Edythe Broad Center for Regeneration Medicine and Stem Cell Research, University of California, San Francisco, CA, USA  
<sup>7</sup>Department of Neuroscience and Physiology, New York University Langone School of Medicine, New York, NY, USA.  
<sup>8</sup>Department of Ophthalmology, New York University Langone School of Medicine, New York, NY, USA.  
<sup>9</sup>Harvard Medical School, Department of Neurobiology, Boston MA, USA  
<sup>*</sup>Corresponding author  
<sup>\#</sup> Equal contribution

### How to run
There are individual scripts for the different parts of the analysis. Run them in this order:
1. Processing Lineage Barcodes
2. Transcriptome analysis and cluster identification
3. Lineage Coupling Analysis

### 1. Processing Lineage Barcodes
The lineage barcodes are trimmed and filtered for quality using the following shell scripts that can be excuted via the terminal. 

##### pre_processing.sh
Custom script written to pre-process FASTQ reads using umi_tools and bbduk. The script performs the following: 

* Selects reads in R2 FASTQ files containing the lineage barcodes. The sequences to the left and right of the lineage barcode are trimmed so that only the lineage barcode remains.
* Lineage barcodes shorter than 55 bp are discarded. 
* Cell barcodes from R1 FASTQ files are extracted using umi_tools to generate a whitelist.
* The extracted cell barcodes and UMIs are added to the read names of the lineage barcode FASTQ files. 

Output:
* lineage barcode FASTQ file 
* Corresponding cell barcode txt file.

##### reformat_Klein.ipynb
 
This simple notebook reformats the files output by pre_processing.sh for input into [LARRY](https://https://github.com/AllonKleinLab/LARRY/blob/master/clonal_annotation.ipynb). 

Output:
* Reformatted lineage barcode FASTQ file with associated cell barcode and UMI. 
* A text file that indicates the sample number. 

##### clonal_annotation.ipynb from [LARRY](https://https://github.com/AllonKleinLab/LARRY/blob/master/clonal_annotation.ipynb)

Default paramaters are kept (i.e. N_READS = 10, N_UMIS =3, N_HAMMING = 3.) Run the script following the instructions of the notebook. 

Output:
* A matrix where rows are cells and columns are clones in csv format.
* The same matrix but in numpy format.
* A txt file of the lineage barcodes. 

##### clone_id_assign.ipynb

A short notebook that takes the output CSV file from LARRY and assigns a clone ID to the lineage barcodes and identifies multi-cell clones (i.e. lineage barcodes with 2 or more cell barcodes assigned to them). Run it in order and as instructed. 

Output:
* A csv file containing the dataset, cell barcode, cloneID and cell_ID. 


### 2. Transcriptome analysis and cluster identification

##### 1-scRNAseq-preprocessing.R
Preprocessing and filtering of CloneSeq scRNA-Seq datasets

##### 2-Cluster identification.R
Cells for which the lineage barcode and transcriptome were recovered were clustered and assigned to major cell-types, followed by analysis of spatial gene expression within each cluster. 

##### 3-Iterative analyses.R
Iterative clustering analyses of microglia, astrocytes, oligodendrocytes and striatal SPNs, and analysis of spatial gene expression.

##### 4-Striatum analysis.R
Uses CellAssign (Zhang et al. Nature Methods (2019)) to annotate dSPNs and iSPNs. Furthermore, striatal clones were integrated with the annotated dataset from Stanley et al. Neuron (2020) in order to assign SPNs as patch or matrix, and dorsolateral or ventromedial.

##### 5-Velocito.R
RNA Velocity (La Manno et al., Nature, 2018) was used to assess maturation trajectories of clonally related oligodendrocytes.


### 3. Lineage Coupling Analysis
There are 2 versions of the Lineage Coupling Analysis. The one used in the main article, derived from Wagner et al. (2018), and the one used in the Supplementary Materials, derived from Weinreb et al. (2020). More information, such as the execution times or usage, can be found in their respectives sub-folders, each of which has its own README file.
The general steps performed in both are:
   1. Load a specified dataset that stores the information of the relations of cells to clones and cell states.
   2. Perform the lineage coupling analysis, given the specified dataset and parameters.
   3. From the latter steps, two result matrices are computed and stored into specified csv files:
      * The matrix that stores the observed metric values for each cell-state pair
      * The matrix that stores the lineage coupling scores for each cell-state pair
   4. The lineage coupling scores matrix is plotted as a clustered heatmap.