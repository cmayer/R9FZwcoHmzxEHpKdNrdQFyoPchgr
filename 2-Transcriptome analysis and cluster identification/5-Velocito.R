source(file = '/data/mayerlab/cmayer/Project_CA/GitLab/cloneseq/lib5.R')
library(SeuratWrappers)
library(loomR)
library(Seurat)
library(velocyto.R)

######## Load processed lineage barcodes 
library(readr)
pool <- read_csv("/data/mayerlab/shared/CloneSeq/results/final_result.csv")
pool$X1 <- NULL
colnames(pool)[colnames(pool) == "cell_ID"] <- "cellID"
pool$dataset <- paste("CA",pool$dataset, sep = '')
pool$cellID <- paste("CA",pool$cellID, sep = '') 
poolraw <- pool 

# load data, set directories and ad metadata if necessarry
dir.create(".../TIJG", showWarnings = FALSE)
dir.create(".../TIJG/results", showWarnings = FALSE)
setwd(".../TIJG")

CA199 <- ReadVelocity(file = ".../CA199/velocyto/CA199.loom")
CA199 <- as.Seurat(x = CA199)
CA199@project.name <- "CA199"
CA199@meta.data$dataset <- "CA199" 

CA203 <- ReadVelocity(file = ".../CA203/velocyto/CA203.loom")
CA203 <- as.Seurat(x = CA203)
CA203@project.name <- "CA203"
CA203@meta.data$dataset <- "CA203" 

CA211 <- ReadVelocity(file = ".../CA211/velocyto/CA211.loom")
CA211 <- as.Seurat(x = CA211)
CA211@project.name <- "CA211"
CA211@meta.data$dataset <- "CA211" 

CA222 <- ReadVelocity(file = ".../CA222/velocyto/CA222.loom")
CA222 <- as.Seurat(x = CA222)
CA222@project.name <- "CA222"
CA222@meta.data$dataset <- "CA222" 

CA233 <- ReadVelocity(file = ".../CA233/velocyto/CA233.loom")
CA233 <- as.Seurat(x = CA233)
CA233@project.name <- "CA233"
CA233@meta.data$dataset <- "CA233" 

CA239 <- ReadVelocity(file = ".../CA239/velocyto/CA239.loom")
CA239 <- as.Seurat(x = CA239)
CA239@project.name <- "CA239"
CA239@meta.data$dataset <- "CA239" 

CA240 <- ReadVelocity(file = ".../CA240/velocyto/CA240.loom")
CA240 <- as.Seurat(x = CA240)
CA240@project.name <- "CA240"
CA240@meta.data$dataset <- "CA240" 

CA241 <- ReadVelocity(file = ".../CA241/velocyto/CA241.loom")
CA241 <- as.Seurat(x = CA241)
CA241@project.name <- "CA241"
CA241@meta.data$dataset <- "CA241" 

CA242 <- ReadVelocity(file = ".../CA242/velocyto/CA242.loom")
CA242 <- as.Seurat(x = CA242)
CA242@project.name <- "CA242"
CA242@meta.data$dataset <- "CA242" 

CA289 <- ReadVelocity(file = ".../CA289/velocyto/CA289.loom")
CA289 <- as.Seurat(x = CA289)
CA289@project.name <- "CA289"
CA289@meta.data$dataset <- "CA289" 

query.list <- c(CA199,CA203,CA211,CA222,CA233,CA239,CA240,CA241,CA242,CA289)

transcriptome.merged <- merge(x = query.list[[1]], y = c(query.list[[2]],query.list[[3]],query.list[[4]],
                     query.list[[5]],query.list[[6]],query.list[[7]],
                       query.list[[8]],query.list[[9]],query.list[[10]]))

####### 
transcriptome.merged <- RenameCells(transcriptome.merged, new.names = gsub(":","_", colnames(x = transcriptome.merged)))
transcriptome.merged <- RenameCells(transcriptome.merged, new.names = gsub("x","", colnames(x = transcriptome.merged)))


###oligo
load(".../TIJC/results/HARMONY_Iterative/cells.oligo.Rdata")
oligo <- subset(transcriptome.merged, cells=cells.oligo, invert = F) 
oligo[["harmony"]] <- CreateDimReducObject(embeddings = emb_har, key = "harmony_", assay = DefaultAssay(oligo))
oligo <- RunUMAP(oligo, dims = 1:15, verbose = T, reduction = "harmony")
oligo <- FindNeighbors(oligo, dims = 1:15, verbose = T,reduction = "harmony")
oligo <- FindClusters(oligo, verbose = T, resolution = 0.2)
Idents(object = oligo) <- as.numeric(Idents(object = oligo)) #remove cluster 0 
Idents(object = oligo) <- factor(Idents(object = oligo), levels = c(1:length(table(Idents(object = oligo))))) 
oligo@meta.data$seurat_clusters <- Idents(object = oligo)
DimPlot(oligo)
oligo <- SCTransform(object = oligo, assay = "spliced")
oligo <- RunPCA(object = oligo, verbose = FALSE)
oligo <- RunVelocity(object = oligo, deltaT = 1, kCells = 25, fit.quantile = 0.02)
ident.colors <- (scales::hue_pal())(n = length(x = levels(x = oligo)))
names(x = ident.colors) <- levels(x = oligo)
cell.colors <- ident.colors[Idents(object = oligo)]
names(x = cell.colors) <- colnames(x = oligo)
pdf("results/velocity_Oligodendrocyte.pdf",width=8,height=8)
show.velocity.on.embedding.cor(emb = Embeddings(object = oligo, reduction = "umap"), vel = Tool(object = oligo, 
         slot = "RunVelocity"), n = 200, scale = "sqrt", cell.colors = ac(x = cell.colors, alpha = 0.5), 
         cex = 0.8, arrow.scale = 3, show.grid.flow = TRUE, min.grid.cell.mass = 0.5, grid.n = 40, arrow.lwd = 1, 
         do.par = FALSE, cell.border.alpha = 0.1)
dev.off()





