### Lineage Coupling Analysis

#### Wagner et al. (2018)

This script performs the following steps:

   1. Load a specified dataset that stores cells along with the individual clones and cell states to which they belong. The source can be a local file or a url. If both or neither are specified, the url source is given priority.
   2. Perform the lineage coupling analysis derived from Wagner et al. (2018), as specified on the Methods section, with a specified number of shufflings.
   3. From the latter steps, 2 result matrices are computed and stored into specified csv files:
      * The matrix that stores the observed metric values for each cell-state pair
      * The matrix that stores the lineage coupling scores for each cell-state pair
   4. The lineage coupling scores matrix is plotted as a clustered heatmap, with euclidean distance as the metric, and a scale resulting from specified lower and upper values.

Usage:

    python lineage_coupling_analysis_wagner_way_5.py [-h] [-N N] [-l csv_data_url] [-f csv_data_file] [-M csv_metric_values_matrix_output_file] [-Z csv_lineage_coupling_scores_matrix_output_file] [-u scale_min] [-v scale_max]

Perform the Lineage Analysis derived from Wagner et al. (2018) for a given input.

Optional arguments:

   | Short version                                     | Long version                                                                                    | Help                                                                                                                                                                           |
   | ------------------------------------------------- | ----------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
   | -h                                                | --help                                                                                          | Show this help message and exit                                                                                                                                                |
   | -N N                                              | --num_shufflings N                                                                              | The number of random shufflings (of cell-type assignments) to be made (default 100)                                                                                            |
   | -l csv_data_url                                   | --csv_url csv_data_url                                                                          | The csv url containing the data (default 'https://keeper.mpdl.mpg.de/lib/011bef7d-af64-4883-9135-afebe1e25e1e/file/results/TIJ/results/lb_pool/TIJ_HARMONY_Clusters.csv?dl=1') |
   | -f csv_data_file                                  | --csv_file csv_data_file                                                                        | The csv file containing the data (default 'Datasets/TIJ_HARMONY_Clusters.csv')                                                                                                 |
   | -M csv_metric_values_matrix_output_file           | --csv_metric_values_matrix_output_file csv_metric_values_matrix_output_file                     | The output csv file where the metric values per state pair will be written (note that the folder must exist) (default 'Results/metric_values_per_state_pair_matrix.csv')      |
   | -Z csv_lineage_coupling_scores_matrix_output_file | --csv_lineage_coupling_scores_matrix_output_file csv_lineage_coupling_scores_matrix_output_file | The output csv file where the lineage coupling scores will be written (note that the folder must exist) (default 'Results/lineage_coupling_scores_matrix.csv')                 |
   | -u scale_min                                      | --clustermap_scale_min scale_min                                                                | The minimum number of the scale used to plot the clustermap (default -5.0)                                                                                                     |
   | -v scale_max                                      | --clustermap_scale_max scale_max                                                                | The maximum number of the scale used to plot the clustermap (default 5.0)                                                                                                      |

Example:

   Compute metric values into 'Results/metric_values_per_state_pair_matrix.csv' and lineage coupling z-scores into 'Results/lineage_coupling_scores_matrix_BioRxiv_Paper.csv', for data in 'Datasets/TIJ_HARMONY_Clusters.csv', with 10,000 shufflings, clustermap_minimum -10.0, and clustermap_maximum 10.0:
   
    python lineage_coupling_analysis_wagner_way_5.py -N 10000 -u -10.0 -v 10.0 -f 'Datasets/TIJ_HARMONY_Clusters.csv' -Z 'Results/lineage_coupling_scores_matrix_Science_Paper.csv' -M 'Results/metric_values_per_state_pair_matrix.csv'

The Datasets and the commands used for obtaining the results for the current project are stored in the 'Datasets' folder and 'commands.txt' file, respectively.

Approximate execution times of the commands in 'commands.txt', in a PC with a 8 GB RAM and AMD Athlon(tm) II X2 250 Processor:

  |        Dataset           |  Execution time  |
  |--------------------------|------------------|
  | TIJ_HARMONY_Clusters     | ca. 50 minutes   |
  | TIJC_HARMONY_Iterative_1 | ca. 45 seconds   |
  | TIJC_HARMONY_Iterative_2 | ca. 50 seconds   |
  | TIJC_HARMONY_Iterative_3 | ca. 1:30 minutes |
  | TIJC_HARMONY_Iterative_4 | ca. 1:10 minutes |
  | TIJC_HARMONY_Iterative_5 | ca. 3:40 minutes |
  | TIJC_HARMONY_Iterative_6 | ca. 30 seconds   |
  | TIJC_HARMONY_Iterative_7 | ca. 20 seconds   |
