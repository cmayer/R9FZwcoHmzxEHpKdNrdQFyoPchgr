#!/bin/sh

#  pre_processing.sh
#  
#
#  Created by May Ho on 17.12.19.
#  

# Load path of bbduk
export PATH=$PATH:/opt/bbmap

# Trimming and selecting reads from R2
# 182
bbduk.sh in1=DI_T_182.conc.R1.fastq.gz in2=DI_T_182.conc.R2.fastq.gz k=11 literal=CTCGAGCTGTG ktrim=r out=stdout.fq int=f skipr1|\
bbduk.sh in1=stdin.fq k=17 literal=GGGGCACCAGCGTATTC hdist=2 edist=2 ktrim=l out1=trim_R1.fastq out2=trim_R2.fastq outm1=dis_R1.fastq outm2=dis_R2.fastq skipr1 maq=15 int=t skipr1
#203
bbduk.sh in1=DI_T_203.conc.R1.fastq.gz in2=DI_T_203.conc.R2.fastq k=11 literal=CTCGAGCTGTG ktrim=r out=stdout.fq int=f skipr1|\
bbduk.sh in1=stdin.fq k=10 literal=GGATCGAATTC ktrim=l out1=trim_R1.fastq out2=trim_R2.fastq outm1=dis_R1.fastq outm2=dis_R2.fastq skipr1 maq=15 int=t skipr1

#222
bbduk.sh in1=DI_T_222.conc.R1.fastq.gz in2=DI_T_222.conc.R2.fastq.gz k=11 literal=CTCGAGCTGTG ktrim=r out=stdout.fq int=f skipr1|\
bbduk.sh in1=stdin.fq k=8 literal=TCGAATTC ktrim=l out1=trim_R1.fastq out2=trim_R2.fastq outm1=dis_R1.fastq outm2=dis_R2.fastq skipr1 maq=15 int=t skipr1

#233
bbduk.sh in1=DI_T_233.conc.R1.fastq.gz in2=DI_T_233.conc.R2.fastq.gz k=11 literal=CTCGAGCTGTG ktrim=r out=stdout.fq int=f skipr1|\
bbduk.sh in1=stdin.fq k=7 literal=AATTATCGAATTC ktrim=l out1=trim_R1.fastq out2=trim_R2.fastq outm1=dis_R1.fastq outm2=dis_R2.fastq skipr1 maq=15 int=t skipr1
#199
bbduk.sh in1=DI_T_199.conc.R1.fastq.gz in2=DI_T_199.conc.R2.fastq.gz k=11 literal=CTCGAGCTGTG ktrim=r out=stdout.fq int=f skipr1|\
bbduk.sh in1=stdin.fq k=17 literal=GGGGCACAAGCGAATTC ktrim=l out1=trim_R1.fastq out2=trim_R2.fastq outm1=dis_R1.fastq outm2=dis_R2.fastq skipr1 maq=15 int=t skipr1
#200, 211, 238, 239, 240, 241, 242, 283, 286, 287, 289
bbduk.sh in1=DI_T_200.conc.R1.fastq.gz in2=DI_T_200.conc.R2.fastq.gz k=11 literal=CTCGAGCTGTG ktrim=r out=stdout.fq int=f skipr1|\
bbduk.sh in1=stdin.fq k=17 literal=AGCGAATTATCGAATTC ktrim=l out1=trim_R1.fastq out2=trim_R2.fastq outm1=dis_R1.fastq outm2=dis_R2.fastq skipr1 maq=15 int=t skipr1

#304, 305, 306
bbduk.sh in1=DI_T_200.conc.R1.fastq.gz in2=DI_T_200.conc.R2.fastq.gz k=11 literal=CTCGAGCTGTG ktrim=r out=stdout.fq int=f skipr1|\
bbduk.sh in1=stdin.fq k=17 literal=GGGGCACAAGCGAATTC ktrim=l out1=trim_R1.fastq out2=trim_R2.fastq outm1=dis_R1.fastq outm2=dis_R2.fastq skipr1 maq=15 int=t skipr1

#308
bbduk.sh in1=DI_T_200.conc.R1.fastq.gz in2=DI_T_200.conc.R2.fastq.gz k=11 literal=CTCGAGCTGTG ktrim=r out=stdout.fq int=f skipr1|\
bbduk.sh in1=stdin.fq k=17 literal=AGCGAATTATCGAATTC ktrim=l out1=trim_R1.fastq out2=trim_R2.fastq outm1=dis_R1.fastq outm2=dis_R2.fastq skipr1 maq=15 int=t skipr1

# Remove short reads and re-pair R1 and R2
bbduk.sh in=trim_R2.fastq out=stdout.fq minlength=55 maxlength=55 | \
repair.sh in1=stdin.fq in2=trim_R1.fastq out1=pair_R1.fastq out2=pair_R2.fastq repair

# Extract cell barcodes using server
umi_tools whitelist --stdin DI_T_308.conc.R1.fastq.gz \ # load the corresponding R1 file
--bc-pattern=CCCCCCCCCCCCCCCCNNNNNNNNNN \
--expect-cells=1152 \
--plot-prefix=expect_whitelist \
--log2stderr > D308_whitelist.txt # change name to the library

# Extract cell barcodes and UMIs and add this to read names
umi_tools extract --bc-pattern=CCCCCCCCCCCCCCCCNNNNNNNNNN \
--stdin pair_R1.fastq \
--stdout pair_R1_extracted.fastq.gz \
--read2-in pair_R2.fastq \
--read2-out=DI_T_309_extracted.fastq \ # change this to the library you're outputing
--filter-cell-barcode \
--whitelist=D309_whitelist.txt; # Note: use the cell bc whitelist generated from the previous step


